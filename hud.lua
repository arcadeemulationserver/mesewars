function mesewars.create_huds_2(player)
  background = player:hud_add({
      hud_elem_type = "image",
      position  = {x = 0.2, y = 0.4},
      offset    = {x = -215, y = 23},
      text      = "mesewars_hud_background.png",
      scale     = { x = 2, y = 2},
      alignment = { x = 1, y = 0 },
    })

    orange = player:hud_add({
      hud_elem_type = "text",
      position  = {x = 0.243, y = 0.325},
      offset    = {x = -215, y = 23},
      text = 'Orange: yes',
      alignment = -1,
      scale     = { x = 50, y = 10},
      number    = 0xffa500,
    })

    blue = player:hud_add({
      hud_elem_type = "text",
      position  = {x = 0.235, y = 0.3},
      offset    = {x = -215, y = 23},
      text = 'Blue: yes',
      alignment = -1,
      scale     = { x = 50, y = 10},
      number    = 0x0000CD,
    })
end

function mesewars.remove_huds_2(player)
  player:hud_remove(blue)
  player:hud_remove(orange)
  player:hud_remove(background)
end

function mesewars.hud_change(hud, to, player)
  player:hud_change(hud, "text", to)
end



function mesewars.hud_update_removed_mese( arena , hud , to )

  -- arena: the arena to update the hud in
  -- hud:   the name of the hud to update
  -- to:    the text to update to
  -- color: the text of the color of the team whose mese was dug
  
  for pl_name, stats in pairs(arena.players) do

    local player = minetest.get_player_by_name(pl_name)

    if player then
  
      mesewars.hud_change( hud , to , player )

    end

  end

end
