-- Code is from WorldEdit
function mesewars.sort_pos(pos1, pos2)
	pos1 = {x=pos1.x, y=pos1.y, z=pos1.z}
	pos2 = {x=pos2.x, y=pos2.y, z=pos2.z}
	if pos1.x > pos2.x then
		pos2.x, pos1.x = pos1.x, pos2.x
	end
	if pos1.y > pos2.y then
		pos2.y, pos1.y = pos1.y, pos2.y
	end
	if pos1.z > pos2.z then
		pos2.z, pos1.z = pos1.z, pos2.z
	end
	return pos1, pos2
end

function mesewars.keep_loaded(pos1, pos2)
	-- Create a vmanip and read the area from map, this
	-- causes all MapBlocks to be loaded into memory.
	-- This doesn't actually *keep* them loaded, unlike the name implies.
	local manip = minetest.get_voxel_manip()
	manip:read_from_map(pos1, pos2)
end

function mesewars.clear_objects(pos1, pos2)
	pos1, pos2 = mesewars.sort_pos(pos1, pos2)

	mesewars.keep_loaded(pos1, pos2)

	local function should_delete(obj)
		-- Avoid players and WorldEdit entities
		if obj:is_player() then
			return false
		end
		local entity = obj:get_luaentity()
		return not entity or not entity.name:find("^worldedit:")
	end

	-- Offset positions to include full nodes (positions are in the center of nodes)
	local pos1x, pos1y, pos1z = pos1.x - 0.5, pos1.y - 0.5, pos1.z - 0.5
	local pos2x, pos2y, pos2z = pos2.x + 0.5, pos2.y + 0.5, pos2.z + 0.5

	local count = 0
	if minetest.get_objects_in_area then
		local objects = minetest.get_objects_in_area({x=pos1x, y=pos1y, z=pos1z},
			{x=pos2x, y=pos2y, z=pos2z})

		for _, obj in pairs(objects) do
			if should_delete(obj) then
				obj:remove()
				count = count + 1
			end
		end
		return count
	end

	-- Fallback implementation via get_objects_inside_radius
	-- Center of region
	local center = {
		x = pos1x + ((pos2x - pos1x) / 2),
		y = pos1y + ((pos2y - pos1y) / 2),
		z = pos1z + ((pos2z - pos1z) / 2)
	}
	-- Bounding sphere radius
	local radius = math.sqrt(
			(center.x - pos1x) ^ 2 +
			(center.y - pos1y) ^ 2 +
			(center.z - pos1z) ^ 2)
	for _, obj in pairs(minetest.get_objects_inside_radius(center, radius)) do
		if should_delete(obj) then
			local pos = obj:get_pos()
			if pos.x >= pos1x and pos.x <= pos2x and
					pos.y >= pos1y and pos.y <= pos2y and
					pos.z >= pos1z and pos.z <= pos2z then
				-- Inside region
				obj:remove()
				count = count + 1
			end
		end
	end
	return count
end






--#########################################################################################
--################          Functions for both minigames            #######################
--#########################################################################################

--===============================
--====   FOR ON_LOAD   ==========
--===============================

-- resets the arena with a schem and clears objects

mesewars.place_arena_schem_clr_obj = function( arena )

    local pos = arena.swpoint
    local path = minetest.get_worldpath() .. "/schems/" .. arena.schemname .. ".mts"
    if minetest.place_schematic(pos, path) == nil then
        minetest.log( "[Mesewars] [arena:".. arena.name .."] ERROR: failed to place Minetest schematic.. check arena properties!")
    end
    mesewars.clear_objects(arena.swpoint, arena.nepoint)

end

--#########################################################################################

--===============================
--====   FOR ON_DEATH  ==========
--===============================

-- when a player dies, if the player's team's mese exists, then respawn the player. Otherwise, eliminate the player.

mesewars.on_death = function( arena , p_name , reason )

    local p_team_id = arena.players[p_name].teamID

	local p_team = arena.teams[ p_team_id ].name




    if arena.teams[ p_team_id ].mese == false then

        -- eliminate player

        arena_lib.remove_player_from_arena( p_name , 1 )
		mesewars.remove_huds_2 ( minetest.get_player_by_name( p_name ))

        -- get the killer's name if possible

        local puncher_name = nil

        if reason and reason.type and reason.type == "punch" and reason.object and reason.object:is_player() then

            puncher_name = reason.object:get_player_name()

        end

        -- make our update message

        local msg = "Player".. p_name .. " was eliminated"

        if puncher_name then 

            msg = msg .. " by " .. puncher_name

        end

        msg = msg .. ", leaving team " .. p_team .. " with " .. tostring( #arenalib.get_players_in_team( arena , p_team_id ) ) .. " players."

        --send our update message to everyone in the minigame

        arena_lib.send_message_in_arena( arena , 'both' , "Player".. p_name .. " was eliminated" )

    else

        -- send the player back to one of his team's spawners if the mese exists

        minetest.get_player_by_name( p_name ):set_pos( arena_lib.get_random_spawner( arena , p_team_id ) )

    end



end

--#########################################################################################





mesewars.on_celebration = function(arena, winner_name)

	for i , _ in pairs(winner_name) do
		mesewars.add_mesecoins(winner_name[i], 100 * arena.mesecoin_weight)
		minetest.chat_send_player(winner_name[i], "You recieved ".. tostring(arena.mesecoin_weight) .." mesecoins. Congrats!")
	end
	
    for pl_name, pl_stats in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        mesewars.remove_huds_2(player)
    end


end