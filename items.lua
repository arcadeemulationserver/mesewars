-- Own Nodes


	minetest.register_node("mesewars:wool_blue", {
			description = "Blue Wool",
			tiles = {"mesewars_wool_blue.png"},
			is_ground_content = false,
			groups = {snappy = 2, choppy = 2, oddly_breakable_by_hand = 3, wool = 1,not_in_creative_inventory = 1},
			-- on_place = function(itemstack, placer, pointed_thing)
     	-- 	if pointed_thing.type == "node" then
      --    		local pos = pointed_thing.above
      --    		minetest.set_node(pos , {name="mesewars:wool_blue"})
			-- 			local inv = placer:get_inventory()
			-- 			inv:remove_item("main", "mesewars:wool_blue")
     	-- 	end
 			-- end,
 			-- on_dig = function(pos, node, digger)
     	-- 	local wielded = digger and digger:get_wielded_item()
  		-- 	local drops = minetest.get_node_drops(node, wielded and wielded:get_name())
     	-- 	minetest.handle_node_drops(pos, drops, digger)
			-- 	minetest.remove_node(pos)
 			-- end,

		})

	minetest.register_node("mesewars:wool_orange", {
  		description = "Orange Wool",
  		tiles = {"mesewars_wool_orange.png"},
  		is_ground_content = false,
  		groups = {snappy = 2, choppy = 2, oddly_breakable_by_hand = 3, wool = 1,not_in_creative_inventory = 1},
			-- on_place = function(itemstack, placer, pointed_thing)
     	-- 	if pointed_thing.type == "node" then
      --    		local pos = pointed_thing.above
      --    		minetest.set_node(pos , {name="mesewars:wool_orange"})
			-- 			local inv = placer:get_inventory()
			-- 			inv:remove_item("main", "mesewars:wool_orange")
     	-- 	end
 			-- end,
 			-- on_dig = function(pos, node, digger)
     	-- 	local wielded = digger and digger:get_wielded_item()
  		-- 	local drops = minetest.get_node_drops(node, wielded and wielded:get_name())
     	-- 	minetest.handle_node_drops(pos, drops, digger)
			-- 	minetest.remove_node(pos)
 			-- end,
  	})

minetest.register_node("mesewars:wood", {
		description = "Mesewars Wood Planks",
		paramtype2 = "facedir",
		place_param2 = 0,
		tiles = {"default_wood.png"},
		is_ground_content = false,
		groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, wood = 1,not_in_creative_inventory = 1},
		-- on_place = function(itemstack, placer, pointed_thing)
		-- 	if pointed_thing.type == "node" then
		-- 			local pos = pointed_thing.above
		-- 			minetest.set_node(pos , {name="mesewars:wood"})
		-- 			local inv = placer:get_inventory()
		-- 			inv:remove_item("main", "mesewars:wood")
		-- 	end
		-- end,
		-- on_dig = function(pos, node, digger)
		-- 	local wielded = digger and digger:get_wielded_item()
		-- 	local drops = minetest.get_node_drops(node, wielded and wielded:get_name())
		-- 	minetest.handle_node_drops(pos, drops, digger)
		-- 	minetest.remove_node(pos)
		-- end,
	})

minetest.register_node("mesewars:stone", {
	description = "Mesewars Stone",
	tiles = {"default_stone.png"},
	groups = {cracky = 3, stone = 1,not_in_creative_inventory = 1},
	drop = "mesewars:stone",
	legacy_mineral = true,
	on_place = function(itemstack, placer, pointed_thing)
		if pointed_thing.type == "node" then
				local pos = pointed_thing.above
				minetest.set_node(pos , {name="mesewars:stone"})
				local inv = placer:get_inventory()
				inv:remove_item("main", "mesewars:stone")
		end
	end,
	on_dig = function(pos, node, digger)
		local wielded = digger and digger:get_wielded_item()
		local drops = minetest.get_node_drops(node, wielded and wielded:get_name())
		minetest.handle_node_drops(pos, drops, digger)
		minetest.remove_node(pos)
	end,
})


-- Dignode



minetest.register_on_dignode(function(pos, oldnode, digger)
	local pl_name = digger:get_player_name()

	if arena_lib.is_player_in_arena(pl_name, "mesewars_2") then
    if not oldnode.name == "mesewars:wool_blue" and not oldnode.name == "mesewars:wool_orange" and not oldnode.name == "mesewars:wool_green" and not oldnode.name == "mesewars:wool_yellow" and not oldnode.name == "default:chest" and not oldnode.name == "mesewars:wood" and not oldnode.name == "mesewars:stone" and not oldnode.name == "default:obsidian" and not oldnode.name == "mesewars:mese_blue" and not oldnode.name == "mesewars:mese_orange" then
      minetest.chat_send_player(pl_name ,"This is a node which you can not pick!")
      minetest.set_node(pos, oldnode)
      local inv = digger:get_inventory()

      local nodeiamon = minetest.get_node(pos)
      if oldnode ~= nil then
        local def = minetest.registered_nodes[oldnode.name]
        if def ~= nil then
          if def.drop ~= nil then
            inv:remove_item("main", def.drop)
          else
            inv:remove_item("main", oldnode)
          end
        end
      end
    end
  end
end)

-- Mese

minetest.register_node("mesewars:mese_blue", {
  description = "Blues Mese",
  groups = {cracky=1, mw_cracky = 1,not_in_creative_inventory = 1},
  tiles = {"mesewars_blue_mese.png"},
  drop = "",
	on_dig = function(pos, node, player)

    local pl_name = player:get_player_name()

    if arena_lib and 
        ( arena_lib.is_player_in_arena(pl_name, "mesewars_2") or arena_lib.is_player_in_arena(pl_name, "mesewars") ) and 
        not ( arena_lib.is_player_spectating(pl_name) ) then

      local arena = arena_lib.get_arena_by_player(pl_name)

      local team_id = arena.players[pl_name].teamID

      local team_name = arena.teams[team_id].name

      if team_name == "blue" then

        minetest.chat_send_player( pl_name , "You can't dig your own mese!")

      else

        minetest.node_dig(pos, node, player)         
        mesewars.add_mesecoins(pl_name, 20)
        minetest.chat_send_player(pl_name, "You recieved 20 mesecoins")

      end

		else

			minetest.node_dig(pos, node, player)

    end

  end,
})



minetest.register_node("mesewars:mese_orange", {
  description = "Oranges Mese",
  groups = {cracky=1, mw_cracky = 1,not_in_creative_inventory = 1},
  tiles = {"mesewars_orange_mese.png"},
  drop = "",
	on_dig = function(pos, node, player)

    local pl_name = player:get_player_name()

    if arena_lib and 
        ( arena_lib.is_player_in_arena(pl_name, "mesewars_2") or arena_lib.is_player_in_arena(pl_name, "mesewars") ) and 
        not ( arena_lib.is_player_spectating(pl_name) ) then

      local arena = arena_lib.get_arena_by_player(pl_name)


      local team_id = arena.players[pl_name].teamID

      local team_name = arena.teams[team_id].name

      if team_name == "orange" then

        minetest.chat_send_player( pl_name , "You can't dig your own mese!")

      else

        minetest.node_dig(pos, node, player)         
        mesewars.add_mesecoins(pl_name, 20)
        minetest.chat_send_player(pl_name, "You recieved 20 mesecoins")

      end

		else

			minetest.node_dig(pos, node, player)

    end

  end,


})



minetest.register_node("mesewars:mese_yellow", {
  description = "Yellows Mese",
  groups = {cracky=1, mw_cracky = 1,not_in_creative_inventory = 1},
  tiles = {"mesewars_yellow_mese.png"},
  drop = "",
	on_dig = function(pos, node, player)

    local pl_name = player:get_player_name()

    if arena_lib and 
        ( arena_lib.is_player_in_arena(pl_name, "mesewars_2") or arena_lib.is_player_in_arena(pl_name, "mesewars") ) and 
        not ( arena_lib.is_player_spectating(pl_name) ) then

      local arena = arena_lib.get_arena_by_player(pl_name)

      local team_id = arena.players[pl_name].teamID

      local team_name = arena.teams[team_id].name

      if team_name == "yellow" then

        minetest.chat_send_player( pl_name , "You can't dig your own mese!")

      else

        minetest.node_dig(pos, node, player)         
        mesewars.add_mesecoins(pl_name, 20)
        minetest.chat_send_player(pl_name, "You recieved 20 mesecoins")

      end

		else

			minetest.node_dig(pos, node, player)

    end

  end,
})



minetest.register_node("mesewars:mese_green", {
  description = "Greens Mese",
  groups = {cracky=1, mw_cracky = 1,not_in_creative_inventory = 1},
  tiles = {"mesewars_blue_mese.png"},
  drop = "",
	on_dig = function(pos, node, player)

    local pl_name = player:get_player_name()

    if arena_lib and 
        ( arena_lib.is_player_in_arena(pl_name, "mesewars_2") or arena_lib.is_player_in_arena(pl_name, "mesewars") ) and 
        not ( arena_lib.is_player_spectating(pl_name) ) then

      local arena = arena_lib.get_arena_by_player(pl_name)

      local team_id = arena.players[pl_name].teamID

      local team_name = arena.teams[team_id].name

      if team_name == "green" then

        minetest.chat_send_player( pl_name , "You can't dig your own mese!")

      else

        minetest.node_dig(pos, node, player)         
        mesewars.add_mesecoins(pl_name, 20)
        minetest.chat_send_player(pl_name, "You recieved 20 mesecoins")

      end

		else

			minetest.node_dig(pos, node, player)

    end

  end,
})





-- Exchanger

minetest.register_craftitem("mesewars:exchanger",{
  description = "Mesewars Exchanger",
  inventory_image = "exchange.png",
})

minetest.register_craftitem("mesewars:coin",{
  description = "Mesewars MeseCoin",
  inventory_image = "Mesecoin.png",
})
-- Drooper


-- Api
function mesewars.register_dropper(dropper, ingot, def)
minetest.register_node(dropper, {
  description = def.material .. "dropper",
  tiles = def.dropper_tiles,
})

minetest.register_craftitem(ingot, {
  description = def.ingot_description,
  inventory_image = def.ingot_image,
})

minetest.register_abm{
  label = def.material .."dropper",
	nodenames = {dropper},
	interval = def.interval,
	chance = 1,
	action = function(pos)
		minetest.spawn_item({x=pos.x, y=pos.y+1, z=pos.z}, ingot)
	end,
}
end

mesewars.register_dropper("mesewars:brickdropper", "mesewars:brick_ingot",{
  material = "Brick",
  dropper_tiles = {"default_brick.png"},
  ingot_description = "Brickingot",
  ingot_image = "default_clay_brick.png",
  interval = 2,
})


mesewars.register_dropper("mesewars:steeldropper", "mesewars:steel_ingot",{
  material = "Steel",
  dropper_tiles = {"default_steel_block.png"},
  ingot_description = "Steelingot",
  ingot_image = "default_steel_ingot.png",
  interval = 10,
})

mesewars.register_dropper("mesewars:golddropper", "mesewars:gold_ingot",{
  material = "Gold",
  dropper_tiles = {"default_gold_block.png"},
  ingot_description = "Goldingot",
  ingot_image = "default_gold_ingot.png",
  interval = 30,
})

mesewars.register_dropper("mesewars:diamonddropper", "mesewars:diamond",{
  material = "Diamond",
  dropper_tiles = {"default_diamond_block.png"},
  ingot_description = "Diamond",
  ingot_image = "default_diamond.png",
  interval = 90,
})


--Shop

minetest.register_node("mesewars:shop_blue", {
  description = "Blue Shop", -- Add image later!
  groups = {unbreakable = 1},
  drop = "",
  paramtype2 = "facedir",
  tiles = {"easyvend_vendor_top_blue.png", "easyvend_vendor_top_blue.png", "easyvend_vendor_side_blue.png", "easyvend_vendor_side_blue.png", "easyvend_vendor_side_blue.png", "easyvend_vendor_front_on_blue.png"},
  on_rightclick = function(pos, node, player, itemstack, pointed_thing)
    local name = player:get_player_name()
    minetest.show_formspec(name, "mesewars:shop_main",
      "size[6,3]" ..
      "item_image_button[0,1;1,1;mesewars:wool_blue;blocksb;]" ..
      "tooltip[blocksb;Blocks]" ..
      "item_image_button[1,1;1,1;default:sword_steel;swords;]" ..
      "tooltip[swords;Swords]" ..
      "item_image_button[2,1;1,1;3d_armor:chestplate_steel;armor;]" ..
      "tooltip[armor;Armor, not aviable now]" ..
      "item_image_button[3,1;1,1;default:pick_steel;tools;]" ..
      "tooltip[tools;Tools]" ..
      "item_image_button[4,1;1,1;default:chest;special;]" ..
      "tooltip[special;Special]" ..
      "image_button[5,1;1,1;healing_potion.png;potions not aviable now,;]" ..
      "tooltip[potions;Potions, not aviable now]" ..
      --[["item_image_button[6,1;1,1;bows:bow;bows not aviable now,;]"..
      "tooltip[bows;Bows, not aviable now]" ..]]
      "item_image_button[0,2;1,1;farming:bread;food not aviable now,;]"..
      "tooltip[food;Food, not aviable now]"..
      "item_image_button[1,2;1,1;mesewars:exchanger;exchange not aviable now,;]"..
      "tooltip[exchange;Exchange, not aviable now]"..
      "item_image_button[2,2;1,1;mesewars:coin;Exchange to Mesecoins not aviable now,;]"..
      "tooltip[mesecoins;Mesecoins, not aviable now]")
  end
})

minetest.register_node("mesewars:shop_orange", {
  description = "Orange Shop",
  groups = {unbreakable = 1},
  drop = "",
  paramtype2 = "facedir",
  tiles = {"easyvend_vendor_top_orange_2.png", "easyvend_vendor_top_orange_2.png", "easyvend_vendor_side_orange_2.png", "easyvend_vendor_side_orange_2.png", "easyvend_vendor_side_orange_2.png", "easyvend_vendor_front_on_orange_2.png"},
  on_rightclick = function(pos, node, player, itemstack, pointed_thing)
    local name = player:get_player_name()
    minetest.show_formspec(name, "mesewars:shop_main",
      "size[6,3]" ..
			--"scroll_container[0,1;5,5;no;horizontal;0.1]"..
      "item_image_button[0,1;1,1;mesewars:wool_orange;blockso;]" ..
      "tooltip[blockso;Blocks]" ..
			--"scrollbar[1,0.2;0.2,1;vertical;scrbar_inv;0.1]" ..
			--"scroll_container_end[]"..
      "item_image_button[1,1;1,1;default:sword_steel;swords;]" ..
      "tooltip[swords;Swords]" ..
      "item_image_button[2,1;1,1;3d_armor:chestplate_steel;armor;]" ..
      "tooltip[armor;Armor, not aviable now]" ..
      "item_image_button[3,1;1,1;default:pick_steel;tools;]" ..
      "tooltip[tools;Tools]" ..
      "item_image_button[4,1;1,1;default:chest;special;]" ..
      "tooltip[special;Special]" ..
      "image_button[5,1;1,1;healing_potion.png;potions,;]" ..
      --[["item_image_button[6,1;1,1;bows:bow;bows;]"..
      "tooltip[bows;Bows, not aviable now]" ..]]
      "item_image_button[0,2;1,1;farming:bread;food;]"..
      "tooltip[food;Food, not aviable now]"..
      "item_image_button[1,2;1,1;mesewars:exchanger;exchange;]"..
      "tooltip[exchange;Exchange, not aviable now]"..
      "item_image_button[2,2;1,1;mesewars:coin;Exchange to Mesecoins ,not aviable now;]"..
      "tooltip[mesecoins;Mesecoins, not aviable now]")
			--"textlist[3,2;1,1;av;hi,hi2,hi3 ]")

  end
})

function mesewars.shop(player, input, output)
  local name = player:get_player_name()
  local inv = player:get_inventory()
  if not inv:contains_item("main", input) then
    minetest.chat_send_player(name, "You don't have enough resouces.")
  elseif not inv:room_for_item("main", output) then
    minetest.chat_send_player(name, "Your inventory does not have enough space.")
  else inv:remove_item("main", input)
    inv:add_item("main", output)
  end
end


minetest.register_on_player_receive_fields(function(player, formname, pressed)
  local name = player:get_player_name()
  if formname == "mesewars:shop_main" then
      if pressed["blocksb"] then
        minetest.show_formspec(name, "mesewars:shop_blocksb",
          "size[9,6]" ..
          "label[1,1;Wool]" ..
          "label[1,2;1 Brick to 3]" ..
          "item_image_button[1,3;1,1;mesewars:wool_blue;4woolb;4:1]" ..
          "item_image_button[1,4;1,1;mesewars:wool_blue;32woolb;32:8]" ..
          "item_image_button[1,5;1,1;mesewars:wool_blue;96woolb;96:24]" ..
					"label[3,1;Stone]" ..
					"label[3,2;1 Brick to 1]" ..
					"item_image_button[3,3;1,1;mesewars:stone;3stone;1:1]" ..
					"item_image_button[3,4;1,1;mesewars:stone;33stone;33:33]" ..
					"item_image_button[3,5;1,1;mesewars:stone;99stone;99:99]" ..
					"label[5,1;Wood]" ..
					"label[5,2;1 Brick to 1]" ..
					"item_image_button[5,3;1,1;mesewars:wood;3wood;1:1]" ..
					"item_image_button[5,4;1,1;mesewars:wood;33wood;33:33]" ..
					"item_image_button[5,5;1,1;mesewars:wood;99wood;99:99]" ..
					--[["label[5,1;Glass]" ..
					"label[5,2;3 Brick to 1]" ..
					"item_image_button[5,3;1,1;default:glass;1glass;1:3]" ..
					"item_image_button[5,4;1,1;default:glass;33glass;33:11]" ..
					"item_image_button[5,5;1,1;default:glass;99glass;99:33]" ..]]
					--[["label[7,1;Steel]" ..
					"label[7,2;3 Steel to 1]" ..
					"item_image_button[7,3;1,1;default:steelblock;1steel;1:3]" ..
					"item_image_button[7,4;1,1;default:steelblock;11steel;11:33]" ..
					"item_image_button[7,5;1,1;default:steelblock;33steel;33:99]" ..]]
					"label[7,1;Obsidian]" ..
					"label[7,2;2 Diamonds to 1]" ..
					"item_image_button[7,3;1,1;default:obsidian;1obs;1:2]" ..
					"item_image_button[7,4;1,1;default:obsidian;11obs;11:22]" ..
					"item_image_button[7,5;1,1;default:obsidian;33obs;33:66]")
        elseif pressed["blockso"] then
            minetest.show_formspec(name, "mesewars:shop_blockso",
              "size[9,6]" ..
              "label[1,1;Wool]" ..
              "label[1,2;1 Brick to 3]" ..
              "item_image_button[1,3;1,1;mesewars:wool_orange;4woolo;4:1]" ..
              "item_image_button[1,4;1,1;mesewars:wool_orange;32woolo;32:8]" ..
              "item_image_button[1,5;1,1;mesewars:wool_orange;96woolo;96:24]" ..
              "label[3,1;Stone]" ..
              "label[3,2;1 Brick to 1]" ..
              "item_image_button[3,3;1,1;mesewars:stone;3stone;1:1]" ..
              "item_image_button[3,4;1,1;mesewars:stone;33stone;33:33]" ..
              "item_image_button[3,5;1,1;mesewars:stone;99stone;99:99]" ..
							"label[5,1;Wood]" ..
              "label[5,2;1 Brick to 1]" ..
              "item_image_button[5,3;1,1;mesewars:wood;3wood;1:1]" ..
              "item_image_button[5,4;1,1;mesewars:wood;33wood;33:33]" ..
              "item_image_button[5,5;1,1;mesewars:wood;99wood;99:99]" ..
              --[["label[5,1;Glass]" ..
              "label[5,2;3 Brick to 1]" ..
              "item_image_button[5,3;1,1;default:glass;1glass;1:3]" ..
              "item_image_button[5,4;1,1;default:glass;33glass;33:11]" ..
              "item_image_button[5,5;1,1;default:glass;99glass;99:33]" ..]]
              --[["label[7,1;Steel]" ..
              "label[7,2;3 Steel to 1]" ..
              "item_image_button[7,3;1,1;default:steelblock;1steel;1:3]" ..
              "item_image_button[7,4;1,1;default:steelblock;11steel;11:33]" ..
              "item_image_button[7,5;1,1;default:steelblock;33steel;33:99]" ..]]
              "label[7,1;Obsidian]" ..
              "label[7,2;2 Diamonds to 1]" ..
              "item_image_button[7,3;1,1;default:obsidian;1obs;1:2]" ..
              "item_image_button[7,4;1,1;default:obsidian;11obs;11:22]" ..
              "item_image_button[7,5;1,1;default:obsidian;33obs;33:66]")

					elseif pressed["blockso"] then
		           minetest.show_formspec(name, "mesewars:shop_blockso",
		             "size[9,6]" ..
		             "label[1,1;Picks]" ..
		             "label[1,2;1 Brick to 3]" ..
		             "item_image_button[1,3;1,1;mesewars:wool_orange;4woolo;4:1]" ..
		             "item_image_button[1,4;1,1;mesewars:wool_orange;32woolo;32:8]" ..
	               "item_image_button[1,5;1,1;mesewars:wool_orange;96woolo;96:24]" ..
								 "label[3,1;Stone]" ..
	               "label[3,2;1 Brick to 1]" ..
	               "item_image_button[3,3;1,1;mesewars:stone;3stone;1:1]" ..
	               "item_image_button[3,4;1,1;mesewars:stone;33stone;33:33]" ..
	               "item_image_button[3,5;1,1;mesewars:stone;99stone;99:99]" ..
	 							"label[5,1;Wood]" ..
	               "label[5,2;1 Brick to 1]" ..
	               "item_image_button[5,3;1,1;mesewars:wood;3wood;1:1]" ..
	               "item_image_button[5,4;1,1;mesewars:wood;33wood;33:33]" ..
	               "item_image_button[5,5;1,1;mesewars:wood;99wood;99:99]" ..
	               --[["label[5,1;Glass]" ..
	               "label[5,2;3 Brick to 1]" ..
	               "item_image_button[5,3;1,1;default:glass;1glass;1:3]" ..
	               "item_image_button[5,4;1,1;default:glass;33glass;33:11]" ..
	               "item_image_button[5,5;1,1;default:glass;99glass;99:33]" ..]]
	               --[["label[7,1;Steel]" ..
	               "label[7,2;3 Steel to 1]" ..
	               "item_image_button[7,3;1,1;default:steelblock;1steel;1:3]" ..
	               "item_image_button[7,4;1,1;default:steelblock;11steel;11:33]" ..
	               "item_image_button[7,5;1,1;default:steelblock;33steel;33:99]" ..]]
	               "label[7,1;Obsidian]" ..
	               "label[7,2;2 Gold to 1]" ..
	               "item_image_button[7,3;1,1;default:obsidian;1obs;1:2]" ..
	               "item_image_button[7,4;1,1;default:obsidian;11obs;11:22]" ..
	               "item_image_button[7,5;1,1;default:obsidian;33obs;33:66]")
			elseif pressed["swords"] then
		     minetest.show_formspec(name, "mesewars:shop_swords",
				   "size[9,4]" ..
		       "label[1,1;Stone Sword]" ..
				   "label[1,2;10 Brick to 1]" ..
		       "item_image_button[1,3;1,1;default:sword_stone;1stonesword;1:10]" ..
           "label[3,1;Steel Sword]" ..
		       "label[3,2;15 Steel to 1]" ..
 	         "item_image_button[3,3;1,1;default:sword_steel;1steelsword;1:15]" ..
				   "label[5,1;Mese Sword]" ..
	       	 "label[5,2;10 Gold to 1]" ..
				   "item_image_button[5,3;1,1;default:sword_mese;1mesesword;1:10]" ..
	       	 "label[7,1;Diamond Sword]" ..
				   "label[7,2;9 Diamonds to 1]" ..
		       "item_image_button[7,3;1,1;default:sword_diamond;1diamondsword;1:9]")
				 elseif pressed["tools"] then
	         minetest.show_formspec(name, "mesewars:shop_tools",
	           "size[9,4]" ..
	           "label[1,1;Stone Pick]" ..
	           "label[1,2;10 Brick to 1]" ..
	           "item_image_button[1,3;1,1;default:pick_stone;1stonepick;1:10]" ..
	           "label[3,1;Steel Pick]" ..
	           "label[3,2;5 Steel to 1]" ..
	           "item_image_button[3,3;1,1;default:pick_steel;1steelpick;1:5]" ..
	           "label[5,1;Diamond Pick]" ..
	           "label[5,2;4 Diamonds to 1]" ..
	           "item_image_button[5,3;1,1;default:pick_diamond;1diamondpick;1:4]" ..
	           "label[7,1;Steel Axe]" ..
	           "label[7,2;1 Diamond to 1]" ..
	           "item_image_button[7,3;1,1;default:axe_steel;1steelaxe;1:1]")

      elseif pressed["special"] then
        minetest.show_formspec(name, "mesewars:shop_special",
          "size[6,4]" ..
          "label[1,1;Chest]" ..
          "label[1,2;2 Gold to 1]" ..
          "item_image_button[1,3;1,1;default:chest;1chest;1:2]" ..
          --"label[3,1;Base Teleporter]" ..
          --"label[3,2;10 Steel to 1]" ..
          --"item_image_button[3,3;1,1;mesewars:baseteleport;1basetp;1:10]" ..
          "label[4,1;Enderpearl]" ..
          "label[4,2;4 Diamond to 1]" ..
          "item_image_button[4,3;1,1;enderpearl:ender_pearl;1pearl;1:4]" )
          --"label[7,1;Bridge Builder]" ..
          --"label[7,2;3 Steel to 1]" ..
          --"item_image_button[7,3;1,1;mesewars:bridge;1bridge;1:3]"..
          --"label[9,1;TNT]" ..
          --"label[9,2;3 Steel to 1]" ..
          --"item_image_button[9,3;1,1;tnt:tnt_burning;1tnt;1:3]")
      elseif pressed["potions"] then
        minetest.show_formspec(name, "mesewars:shop_potion",
          "size[9,4]" ..
          "label[1,1;Regen Potion]" ..
          "label[1,2;1 Gold to 1]" ..
          "item_image_button[1,3;1,1;pep:regen;1regen;1:1]" ..
          "label[3,1;Regen Potion2]" ..
          "label[3,2;2 Gold to 1]" ..
          "item_image_button[3,3;1,1;pep:regen2;1regen2;1:2]" ..
          "label[5,1;Speed Potion]" ..
          "label[5,2;15 Steel to 1]" ..
          "item_image_button[5,3;1,1;pep:speedplus;1speed;1:15]" ..
          "label[7,1;Jump Potion]" ..
          "label[7,2;5 Steel to 1]" ..
          "item_image_button[7,3;1,1;pep:jumpplus;1jump;1:5]")
      elseif pressed["bows"] then
        minetest.show_formspec(name, "mesewars:shop_bows",
          "size[5,4]" ..
          "label[1,1;Bow]" ..
          "label[1,2;1 Gold to 1]" ..
          "item_image_button[1,3;1,1;bow:bow;1bow;1:4]" ..
          "label[3,1;Arrow]" ..
          "label[3,2;25 Brick to 5]" ..
          "item_image_button[3,3;1,1;bow:arrow;5arrow;5:25]")
      elseif pressed["food"] then
        minetest.show_formspec(name, "mesewars:shop_food",
          "size[7,4]" ..
          "label[1,1;Mushroom]" ..
          "label[1,2;1 Brick to 10]" ..
          "item_image_button[1,3;1,1;flowers:mushroom_brown;10mushroom;1:10]" ..
          "label[3,1;Apple]" ..
          "label[3,2;1 Brick to 5]" ..
          "item_image_button[3,3;1,1;default:apple;5apple;1:5]"..
          "label[5,1;Bread]" ..
          "label[5,2;1 Steel to 5]" ..
          "item_image_button[5,3;1,1;farming:bread;5bread;1:5]")
      elseif pressed["exchange"] then
        minetest.show_formspec(name, "mesewars:shop_exchange",
          "size[13,6]" ..
          "label[1,3;Buy]" ..
          "label[1,5;Sell]" ..
          "label[3,1;Steel and Brick]" ..
          "label[3,2;5 Bricks = 1 Steel]" ..
          "item_image_button[3,3;1,1;mesewars:steel_ingot;bs;7:1]" ..
          "item_image_button[3,5;1,1;mesewars:brick_ingot;sb;1:5]" ..
          "label[6,1;Gold and Steel]" ..
          "label[6,2;3 Steel = 1 Gold]" ..
          "item_image_button[6,3;1,1;mesewars:gold_ingot;sg;5:1]" ..
          "item_image_button[6,5;1,1;mesewars:steel_ingot;gs;1:3]" ..
          "label[9,1;Diamond and Gold]" ..
          "label[9,2;3 Gold = Diamond]" ..
          "item_image_button[9,3;1,1;mesewars:diamond;gd;5:1]" ..
          "item_image_button[9,5;1,1;mesewars:gold_ingot;dg;1:3]")
      end
  elseif formname == "mesewars:shop_blocksb" then
    if pressed["4woolb"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:wool_blue 4")
    elseif pressed["32woolb"] then
      mesewars.shop(player, "mesewars:brick_ingot 8", "mesewars:wool_blue 32")
    elseif pressed["96woolb"] then
      mesewars.shop(player, "mesewars:brick_ingot 24", "mesewars:wool_blue 96")
		elseif pressed["3stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:stone 1")
    elseif pressed["33stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "mesewars:stone 33")
    elseif pressed["99stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "mesewars:stone 99")
		elseif pressed["3wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:wood 1")
    elseif pressed["33wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "mesewars:wood 33")
    elseif pressed["99wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "mesewars:wood 99")
    elseif pressed["3sand"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "default:sandstone 1")
    elseif pressed["33sand"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "default:sandstone 33")
    elseif pressed["99sand"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "default:sandstone 99")
    elseif pressed["1glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "default:glass 1")
    elseif pressed["33glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "default:glass 11")
    elseif pressed["99glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "default:glass 33")
    elseif pressed["1steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 3", "default:steelblock 1")
    elseif pressed["11steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 33", "default:steelblock 11")
    elseif pressed["33steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 99", "default:steelblock 33")
    elseif pressed["1obs"] then
      mesewars.shop(player, "mesewars:diamond 2", "default:obsidian 1")
    elseif pressed["11obs"] then
      mesewars.shop(player, "mesewars:diamond 22", "default:obsidian 11")
    elseif pressed["33obs"] then
      mesewars.shop(player, "mesewars:diamond 66", "default:obsidian 33")
    end

  elseif formname == "mesewars:shop_blockso" then
    if pressed["4woolo"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:wool_orange 4")
    elseif pressed["32woolo"] then
      mesewars.shop(player, "mesewars:brick_ingot 8", "mesewars:wool_orange 32")
    elseif pressed["96woolo"] then
      mesewars.shop(player, "mesewars:brick_ingot 24", "mesewars:wool_orange 96")
    elseif pressed["3stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:stone 1")
    elseif pressed["33stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "mesewars:stone 33")
    elseif pressed["99stone"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "mesewars:stone 99")
		elseif pressed["3wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "mesewars:wood 1")
    elseif pressed["33wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "mesewars:wood 33")
    elseif pressed["99wood"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "mesewars:wood 99")
    elseif pressed["1glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "default:glass 1")
    elseif pressed["33glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 33", "default:glass 11")
    elseif pressed["99glass"] then
      mesewars.shop(player, "mesewars:brick_ingot 99", "default:glass 33")
    elseif pressed["1steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 3", "default:steelblock 1")
    elseif pressed["11steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 33", "default:steelblock 11")
    elseif pressed["33steel"] then
      mesewars.shop(player, "mesewars:steel_ingot 99", "default:steelblock 33")
    elseif pressed["1obs"] then
      mesewars.shop(player, "mesewars:diamond 2", "default:obsidian 1")
    elseif pressed["11obs"] then
      mesewars.shop(player, "mesewars:diamond 22", "default:obsidian 11")
    elseif pressed["33obs"] then
      mesewars.shop(player, "mesewars:diamond 66", "default:obsidian 33")
    end

	elseif formname == "mesewars:shop_swords" then
    if pressed["1stonesword"] then
      mesewars.shop(player, "mesewars:brick_ingot 10", "default:sword_stone")
    elseif pressed["1steelsword"] then
      mesewars.shop(player, "mesewars:steel_ingot 15", "default:sword_steel")
    elseif pressed["1mesesword"] then
      mesewars.shop(player, "mesewars:gold_ingot 10", "default:sword_mese")
    elseif pressed["1diamondsword"] then
      mesewars.shop(player, "mesewars:diamond 9", "default:sword_diamond")
    end

  elseif formname == "mesewars:shop_armor" then
    if pressed["1woodh"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "3d_armor:helmet_wood")
    elseif pressed["1woodc"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "3d_armor:chestplate_wood")
    elseif pressed["1woodl"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "3d_armor:leggings_wood")
    elseif pressed["1woodb"] then
      mesewars.shop(player, "mesewars:brick_ingot 3", "3d_armor:boots_wood")
    elseif pressed["1cactush"] then
      mesewars.shop(player, "mesewars:steel_ingot 2", "3d_armor:helmet_cactus")
    elseif pressed["1cactusc"] then
      mesewars.shop(player, "mesewars:steel_ingot 2", "3d_armor:chestplate_cactus")
    elseif pressed["1cactusl"] then
      mesewars.shop(player, "mesewars:steel_ingot 2", "3d_armor:leggings_cactus")
    elseif pressed["1cactusb"] then
      mesewars.shop(player, "mesewars:steel_ingot 2", "3d_armor:boots_cactus")
    elseif pressed["1steelh"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "3d_armor:helmet_steel")
    elseif pressed["1steelc"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "3d_armor:chestplate_steel")
    elseif pressed["1steell"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "3d_armor:leggings_steel")
    elseif pressed["1steelb"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "3d_armor:boots_steel")
    elseif pressed["1diah"] then
      mesewars.shop(player, "mesewars:diamond 3", "3d_armor:helmet_diamond")
    elseif pressed["1diac"] then
      mesewars.shop(player, "mesewars:diamond 3", "3d_armor:chestplate_diamond")
    elseif pressed["1dial"] then
      mesewars.shop(player, "mesewars:diamond 3", "3d_armor:leggings_diamond")
    elseif pressed["1diab"] then
      mesewars.shop(player, "mesewars:diamond 3", "3d_armor:boots_diamond")
    end
  elseif formname == "mesewars:shop_tools" then
    if pressed["1stonepick"] then
      mesewars.shop(player, "mesewars:brick_ingot 10", "default:pick_stone")
    elseif pressed["1stoneaxe"] then
      mesewars.shop(player, "mesewars:brick_ingot 5", "default:axe_stone")
    elseif pressed["1stoneshovel"] then
      mesewars.shop(player, "mesewars:brick_ingot 5", "default:shovel_stone")
    elseif pressed["1steelpick"] then
      mesewars.shop(player, "mesewars:steel_ingot 5", "default:pick_steel")
    elseif pressed["1steelaxe"] then
      mesewars.shop(player, "mesewars:diamond 1", "default:axe_steel")
    elseif pressed["1steelshovel"] then
      mesewars.shop(player, "mesewars:steel_ingot 5", "default:shovel_steel")
    elseif pressed["1mesepick"] then
      mesewars.shop(player, "mesewars:gold_ingot 5", "default:pick_mese")
    elseif pressed["1meseaxe"] then
      mesewars.shop(player, "mesewars:gold_ingot 5", "default:axe_mese")
    elseif pressed["1meseshovel"] then
      mesewars.shop(player, "mesewars:gold_ingot 5", "default:shovel_mese")
    elseif pressed["1diamondpick"] then
      mesewars.shop(player, "mesewars:diamond 4", "default:pick_diamond")
    elseif pressed["1diamondaxe"] then
      mesewars.shop(player, "mesewars:diamond 2", "default:axe_diamond")
    elseif pressed["1diamondshovel"] then
      mesewars.shop(player, "mesewars:diamond 2", "default:shovel_diamond")
    end
  elseif formname == "mesewars:shop_special" then
    if pressed["1chest"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "default:chest")
    elseif pressed["1basetp"] then
      mesewars.shop(player, "mesewars:steel_ingot 10", "mesewars:baseteleport")
    elseif pressed["1pearl"] then
      mesewars.shop(player, "mesewars:diamond 4", "enderpearl:ender_pearl")
    elseif pressed["1bridge"] then
      mesewars.shop(player, "mesewars:steel_ingot 3", "mesewars:bridge")
    elseif pressed["1tnt"] then
      mesewars.shop(player, "mesewars:steel_ingot 3", "tnt:tnt_burning")
    end
  elseif formname == "mesewars:shop_potion" then
    if pressed["1regen"] then
      mesewars.shop(player, "mesewars:gold_ingot 1", "pep:regen")
    elseif pressed["1regen2"] then
      mesewars.shop(player, "mesewars:gold_ingot 2", "pep:regen2")
    elseif pressed["1speed"] then
      mesewars.shop(player, "mesewars:steel_ingot 15", "pep:speedplus")
    elseif pressed["1jump"] then
      mesewars.shop(player, "mesewars:steel_ingot 5", "pep:jumpplus")
    end
  elseif formname == "mesewars:shop_bows" then
    if pressed["1bow"] then
      mesewars.shop(player, "mesewars:gold_ingot 1", "bow:bow")
    elseif pressed["5arrow"] then
      mesewars.shop(player, "mesewars:brick_ingot 25", "bow:arrow 5")
    end
  elseif formname == "mesewars:shop_food" then
    if pressed["10mushroom"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "flowers:mushroom_brown 10")
    elseif pressed["5apple"] then
      mesewars.shop(player, "mesewars:brick_ingot 1", "default:apple 5")
    elseif pressed["5bread"] then
      mesewars.shop(player, "mesewars:steel_ingot 1", "farming:bread 5")
    end
  end
end)
