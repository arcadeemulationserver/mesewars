--[[

  in the game, we need to do the following:
  
    if a team has its meseblock, its players can respawn at the respawn point. If it has lost its meseblock then its players  cannot respawn. 
    So every tick, check if the meseblock is at the location. if it isnt then set mese to false for that team.
        
    the functions that are used for both minigames can be found in functions.lua The functions for HUD can be found in hud.lua

    on_load sets the arena and removes object

    on_start sets up the hud

    on_death checks to see if the teams' mese exists, if it does, the player respawns, if not, they are eliminated

    on_tick checks to see if the node at arena.<color>_mese is that color team's mese, if it isnt then huds are updated, chat is sent to all players
        that the mese was dug, and the team property "mese" is set to false so no more players will respawn from that team.


]]




--####################################################################
--################          ON LOAD            #######################
--####################################################################

arena_lib.on_load('mesewars_2', function(arena)

    mesewars.place_arena_schem_clr_obj(arena)

end)

arena_lib.on_load('mesewars', function(arena)

    mesewars.place_arena_schem_clr_obj(arena)

end)

--####################################################################
--################          ON START           #######################
--####################################################################


arena_lib.on_start('mesewars_2', function(arena)

    --Show HUD to players , TODO, make a graphical HUD instead of just "true/false, also put it in the lower left corner to be out of the way"

    for pl_name, stats in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        mesewars.create_huds_2(player)
    end

end)


-- arena_lib.on_start('mesewars', function(arena)

--     --#######  TODO #######
--     --Show HUD for 4 player mesewars

--     -- for pl_name, stats in pairs(arena.players) do
--     --     local player = minetest.get_player_by_name(pl_name)
--     --     mesewars.create_huds_2(player)
--     -- end

-- end)



--####################################################################
--################          ON DEATH           #######################
--####################################################################


arena_lib.on_death('mesewars_2', function(arena, p_name, reason)

    mesewars.on_death( arena , p_name , reason )

end)

arena_lib.on_death('mesewars', function(arena, p_name, reason)

    mesewars.on_death( arena , p_name , reason )

end)


--####################################################################
--################          ON TIME_TICK       #######################
--####################################################################


arena_lib.on_time_tick('mesewars_2', function(arena)



    local orange_pos = arena.orange_mese

    local blue_pos = arena.blue_mese

    local orange_id

    local blue_id


    for id, stats in pairs(arena.teams) do
        if stats.name == "orange" then
            orange_id = id
        end
        if stats.name == "blue" then
            blue_id = id
        end
    end


    if not minetest.get_node( orange_pos ) .name() ==  "mesewars:mese_orange" then

        arena.teams[orange_id].mese = false 

        mesewars.hud_update_removed_mese( arena , orange , "Orange: no" )
        arena_lib.send_message_in_arena( arena , 'both' , "Orange's Mese was dug!" )

    end


    if not minetest.get_node( blue_pos ) .name() ==  "mesewars:mese_blue" then

        arena.teams[blue_id].mese = false 

        mesewars.hud_update_removed_mese( arena , blue , "Blue: no")
        arena_lib.send_message_in_arena( arena , 'both' , "'s Mese was dug!" )

    end


end)


arena_lib.on_time_tick('mesewars', function(arena)


    local orange_pos = arena.orange_mese

    local blue_pos = arena.blue_mese

    local yellow_pos = arena.yellow_mese

    local green_pos = arena.green_mese

    local orange_id
    local blue_id
    local yellow_id
    local green_id


    for id, stats in pairs(arena.teams) do
        if stats.name == "orange" then
            orange_id = id
        end
        if stats.name == "blue" then
            blue_id = id
        end
        if stats.name == "yellow" then
            yellow_id = id
        end
        if stats.name == "green" then
            green_id = id
        end
    end


    if not minetest.get_node( orange_pos ) .name() ==  "mesewars:mese_orange" then

        arena.teams[orange_id].mese = false 

        --mesewars.hud_update_removed_mese( arena , orange , "Orange: no")
        arena_lib.send_message_in_arena( arena , 'both' , "Orange's Mese was dug!" )

    end


    if not minetest.get_node( blue_pos ) .name() ==  "mesewars:mese_blue" then

        arena.teams[blue_id].mese = false 

        --mesewars.hud_update_removed_mese( arena , blue , "Blue: no" )
        arena_lib.send_message_in_arena( arena , 'both' , "Blue's Mese was dug!" )

    end

    if not minetest.get_node( yellow_pos ) .name() ==  "mesewars:mese_yellow" then

        arena.teams[yellow_id].mese = false 

        --mesewars.hud_update_removed_mese( arena , yellow , "Yellow: no" )
        arena_lib.send_message_in_arena( arena , 'both' , "Yellow's Mese was dug!" )

    end

    if not minetest.get_node( green_pos ) .name() ==  "mesewars:mese_green" then

        arena.teams[green_id].mese = false 

        --mesewars.hud_update_removed_mese( arena , green , "Green: no" )
        arena_lib.send_message_in_arena( arena , 'both' , "Green's Mese was dug!" )

    end
    
end)




--####################################################################
--################        ON CELEBRATION        ######################
--####################################################################






arena_lib.on_celebration('mesewars_2', function(arena, winner_name)
    mesewars.on_celebration(arena, winner_name)
end)


arena_lib.on_celebration('mesewars', function(arena, winner_name)
    mesewars.on_celebration(arena, winner_name)
end)

